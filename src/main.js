import { createApp } from "vue";
import App from "./App.vue";
import "./assets/global.css";
import router from "./router";
import VueAxios from "vue-axios";
import axios from "axios";
import store from "./store";

const app = createApp(App);
app.use(router);
app.use(VueAxios, axios);
app.use(store);
app.mount("#app");
