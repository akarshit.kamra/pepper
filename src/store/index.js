import createLink from "./modules/createLink";
import { createStore } from "vuex";
//Load Vuex

const store = createStore({
  modules: {
    createLink,
  },
});

export default store;
