import axios from "axios";

const state = {
  creatorForm: false,
  campaignForm: false,
  creators: [],
  brandCategories: [],
  brands: [],
  campaigns: [],
  products: [],
  pebbles: [],
};

const getters = {
  allCreators: (state) => state.creators,
  getCategories: (state) => state.brandCategories,
  getBrands: (state) => state.brands,
  getCampaigns: (state) => state.campaigns,
  getProducts: (state) => state.products,
  getPebbles: (state) => state.pebbles,
  createCreator: (state) => state.creatorForm,
  createCampaign: (state) => state.campaignForm,
};

const actions = {
  async getData({ commit }) {
    const creators = await axios.get("http://13.126.197.100/creator");
    const campaigns = await axios.get("http://13.126.197.100/campaigndetails");
    const pebbles = await axios.get("http://13.126.197.100/pebbles");

    commit("setCreators", creators.data.content);
    commit("setCampaigns", campaigns.data.content);
    commit("setPebbles", pebbles.data.content);
  },
  async fetchCreators({ commit }) {
    const creators = await axios.get("http://13.126.197.100/creator");
    commit("setCreators", creators.data.content);
  },

  async fetchBrandCategories({ commit }) {
    const response = await axios.get("http://13.126.197.100/brandcategory");
    commit("setCategories", response.data.content);
  },
  async fetchBrands({ commit }) {
    const response = await axios.get("http://13.126.197.100/brand");
    commit("setBrands", response.data.content);
  },
  async fetchCampaigns({ commit }) {
    const response = await axios.get("http://13.126.197.100/campaigndetails");
    commit("setCampaigns", response.data.content);
    response.data.content.forEach((e) => {
      console.log(e.creator_details._id);
    });
  },
  async fetchProducts({ commit }) {
    const response = await axios.get("http://13.126.197.100/product");
    commit("setProducts", response.data.content);
  },
  async fetchPebbles({ commit }) {
    const response = await axios.get("http://13.126.197.100/pebbles");
    commit("setPebbles", response.data.content);
  },
  async toggleCreatorForm({ commit }, check) {
    commit("setCreatorForm", check);
  },
  async toggleCampaignForm({ commit }, check) {
    commit("setCampaignForm", check);
  },
  async createNewCreator({ commit }, creator_details) {
    let details = {
      name: creator_details.name,
      email: creator_details.email,
      phone_number: creator_details.phone_number,
      address: creator_details.address,
      category: creator_details.category,
      activation_store: new Boolean(creator_details.activation_store),
      store_link: creator_details.store_link,
      budget_provided: creator_details.budget_provided,
      gmv: creator_details.gmv,
    };
    await axios.post("http://13.126.197.100/creator", JSON.stringify(details));
    commit("createNewCreator", creator_details);
    commit("setCreatorForm", false);
    location.reload();
  },
  async createNewCampaign(_, campaign_details) {
    // let details = {
    //   name: campaign_details.name,
    //   type: campaign_details.type,
    //   start_date: new Date(campaign_details.start_date),
    //   end_date: new Date(
    //     campaign_details.end_date ? campaign_details.end_date : null
    //   ),
    //   creator_details: campaign_details.creator_id,
    // };
    // await axioxs.post(
    //   "http://13.126.197.100/campaigndetails",
    //   JSON.stringify(details)
    // );
    // commit("createNewCampaign", details);
    console.log(campaign_details);
  },
};

const mutations = {
  setCreators: (state, creators) => {
    state.creators = creators;
  },
  setCategories: (state, brandCategories) => {
    state.brandCategories = brandCategories;
  },
  setBrands: (state, brands) => {
    state.brands = brands;
  },
  setCampaigns: (state, campaigns) => {
    state.campaigns = campaigns;
  },
  setProducts: (state, products) => {
    state.products = products;
  },
  setPebbles: (state, pebbles) => {
    state.pebbles = pebbles;
  },
  setCreatorForm: (state, creatorForm) => {
    state.creatorForm = creatorForm;
  },
  setCampaignForm: (state, campaignForm) => {
    state.campaignForm = campaignForm;
  },
  createNewCreator: (state, creator_details) => {
    state.creators.push(creator_details);
  },
  createNewCampaign: (state, campaign_details) => {
    state.campaigns.push(campaign_details);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
