import { createWebHistory, createRouter } from "vue-router";

import Home from "./views/HomeView.vue";
import ManagementComp from "./views/ManagementComp.vue";
import CampaignsPage from "./views/CampaignsPage.vue";
// import OrderDetails from "./views/OrderDetails.vue";
import Settings from "./views/ProfileSettings.vue";
import Catalog from "./views/CatalogView.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/manager",
    name: "ManagementComp",
    component: ManagementComp,
  },
  {
    path: "/creator_id=:id",
    name: "CampaignsPage",
    component: CampaignsPage,
  },
  {
    path: "/orders",
    name: "OrderDetails",
    component: ManagementComp,
  },
  {
    path: "/settings",
    name: "ProfileSettings",
    component: Settings,
  },
  {
    path: "/catalog",
    name: "Catalog",
    component: Catalog,
    props: true,
  },
];

const router = new createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
